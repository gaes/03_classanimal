export class Animal {
	//field 
	edad: number;

	//constructor 
	constructor(edad: number) {
		this.edad = edad;
	}

	//function 
	public crecer(): number {
		this.edad = this.edad + 1;
		return this.edad;
	}
}