import { Component } from '@angular/core';
import { Animal } from './classes/animal';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[]
})
export class AppComponent { 
  
  title;
  
  constructor() {
    var x = new Animal(6);
    this.title  = x.crecer().toString();
    console.log(this.title);
  }  
 
}
